require 'json'
require 'net/http'
require 'colorize'

SCALE = 2

def renderBar(temp)
  temp = temp.to_i

  if temp >= 0
    bar = ("\u2582" * 40 * SCALE) + "\u2592" + ("\u2588" * temp.abs * SCALE) + ("\u2582" * (40 - temp.abs) * SCALE)
  else
    bar = ("\u2582" * (40 - temp.abs) * SCALE) + ("\u2588" * temp.abs * SCALE) + "\u2592" + ("\u2582" * 40 * SCALE)
  end

  if temp > 25
    bar = bar.red
  elsif temp > 20
    bar = bar.yellow
  elsif temp > 10
    bar = bar.light_green
  elsif temp > 0
    bar = bar.light_blue
  elsif temp > -10
    bar = bar.blue
  else
    bar = bar.magenta
  end
end

def printFrame(frame)
  getDate = lambda { |time| Time.at(time).strftime("%A, %B %d") }
  getTime = lambda { |time| Time.at(time).strftime("%H:%M") }

  avgtemp = ((frame["temperatureHigh"].to_i + frame["temperatureLow"].to_i) / 2).to_i
  bar     = renderBar(avgtemp)

  puts getDate.call(frame["time"])
  puts "\t#{frame["summary"]}"

  high = ("%-10s %9sC" % ["High:", frame["temperatureHigh"]]).red
  low  = ("%-10s %9sC" % ["Low:", frame["temperatureLow"]]).blue
  avg  = ("%-10s %9sC" % ["Avg:", avgtemp]).yellow

  humidity = ((frame["humidity"].to_f) * 100).to_i

  puts "\t%s   %s    %s" % [low, avg, high]
  puts "\t%-10s %10s   %-10s %10s    %-10s%10s%%" % ["Sunrise:", getTime.call(frame["sunriseTime"]), "Sunset:", getTime.call(frame["sunsetTime"]), "Humidity:", humidity]
  puts bar
end

ds_api       = ENV["DARK_SKY_API_KEY"]
ds_loc       = ENV["DARK_SKY_LAT_LONG"]

raise "Please set \"DARK_SKY_API_KEY\" to your Dark Sky API Key and \"DARK_SKY_LAT_LONG\" to the lat/long of a location" if ds_api == nil || ds_loc == nil

json         = Net::HTTP.get(URI("https://api.darksky.net/forecast/#{ds_api}/#{ds_loc}?units=uk2"))
weather_data = JSON.parse(json)

puts "Currently it is #{weather_data["currently"]["temperature"]}C"
puts weather_data["daily"]["summary"]
puts renderBar(weather_data["currently"]["temperature"].to_i)
puts ""

weather_data["daily"]["data"].each {|day| printFrame(day)}
puts "\nPowered by Dark Sky"
